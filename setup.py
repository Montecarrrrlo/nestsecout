from setuptools import setup, find_packages
from os import path
from io import open

BASE_DIR = path.abspath(path.dirname(__file__))

INSTALL_REQUIRED = [
    "numpy>1.15.0, <2.0.0",
    "scipy>1.2.0, <1.3.0",
    "requests>2.18.0, <3.0.0",
]

# Get the long description from the README file
with open(path.join(BASE_DIR, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='nestsecout',
    version='0.0.1-alpha',
    description='NEST Secure Outsourcing Implementation',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/Montecarrrrlo/nestsecout',
    author='Montecarrrrlo',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'testdata']),

    python_requires='>=3.0, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, <4',

    install_requires=INSTALL_REQUIRED

)
