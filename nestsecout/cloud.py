# -*- coding: utf-8 -*-
"""
This module contains the implementation of matrix data types such that time consuming operations
is done on the cloud. The intention of the project is to support the implementation of the Secure
Outsourcing project of NEST in CWRU.
"""
import os
import warnings
from tempfile import NamedTemporaryFile

import numpy as np
import requests


class APIError(Exception):
    """
    Error happens when the communication with cloud is unexpected
    """
    pass


DEFAULT_URL = 'http://127.0.0.1:8000'
SUPPORTED_TYPE = ('Numpy',)
_USAGE_MESSAGE = """ge : 
>>> CloudMatrix(<url>, <id>);
 or
>>> CloudMatrix(<url>, <filepath>, <format>)
"""


class CloudMatrix(object):
    """ This class is an implementation of matrix type that matrix operation is done in the cloud.

    The direct use of this class is **strongly unadvised** since this Class is only a wrapper over
    the Cloud API (as implemented in rest-matrix>.
    For a easier usage, please use `cloud.CloudMatrixManager`.

    Attributes:
        url (str): the url that the cloud API is running on
        id (int): the primary key reference of the matrix in the cloud
        filepath (str): the file path to a file reference to matrix in local file storage
        format (str): any supported format as documented in `cloud.SUPPORTED_FORMAT'.
                        Must be supplied when filepath is specified.

    Examples:
        >>> tf = NamedTemporaryFile(suffix=".npy", delete=False)
        >>> tf.close()
        >>> np.save(tf, np.asarray([[1,2,3,4], [5,6,7,8]]))
        >>> cm = CloudMatrix(filepath=tf.name, format='Numpy')

        - Generate matrix from local file storage
        >>> cm.to_array()
        array([[1, 2, 3, 4],
               [5, 6, 7, 8]])

        - Reference to an existing matrix in the cloud
        >>> cm2 = CloudMatrix(id=cm.id)
        >>> cm2.to_array()
        array([[1, 2, 3, 4],
               [5, 6, 7, 8]])

        - Perform matrix transpose
        >>> cm3 = cm2.transpose()
        >>> cm3.to_array()
        array([[1, 5],
               [2, 6],
               [3, 7],
               [4, 8]])

        - Perform matrix multiplication
        >>> cm4 = cm2.multiply(cm3)  # A @ A.T
        >>> cm4.to_array()
        array([[ 30,  70],
               [ 70, 174]])

        - Clean up matrix instances on the cloud
        *IMPORTANT*: The matrices in the cloud storage will not be cleaned up automatically.
                You MUST delete them manually as shown as follows.
        >>> c.delete for c in [cm, cm2, cm3, cm4]

        - Delete the temporary file created
        >>> os.remove(tf.name)
    """

    def __init__(self, url=DEFAULT_URL, id=None, filepath=None, format=None):
        """
        Initializes a CloudMatrix from either the location from the cloud or a local file

        Examples :
            >>> CloudMatrix(url="127.0.0.1:8000", id=1882)
             or
            >>> CloudMatrix(url='127.0.0.1:8000', filepath="X://y/z.npy", format="Numpy")
        :param url: the url reference to the cloud
        :type  url: str
        :param id: the primary key reference to the specific matrix on cloud
        :type  id: int
        :param filepath: the file path to local
        :type  filepath: str
        :param format:
        :type  format: str
        """
        if not url:
            raise ValueError("The url to the matrix must be given.")
        # obtain only the base url
        elif url.startswith('http://') or url.startswith('https://'):
            self.url = '/'.join(url.split('/')[0:3])
        else:
            self.url = 'http://' + url.split('/')[0]

        if (id and filepath) or (not id and not filepath):
            raise ValueError('Either id or filepath have to be supplied.\n{:s}'.format(_USAGE_MESSAGE))

        if id:
            self.id = id
            response = requests.get(
                url='{:s}/matrix/{:d}'.format(self.url, self.id)
            )
            if response.status_code == 404:
                self.deleted = True
                warnings.warn(
                    message="matrix {:d} is either deleted or does not exist in the cloud.",
                    category=Warning
                )
            else:
                self.deleted = False
                self.format = response.json()['format']
        elif filepath and format:
            if format not in SUPPORTED_TYPE:
                raise ValueError("Unsupported matrix format.")
            data = {"format": format}
            files = {'file': (os.path.basename(filepath), open(filepath, 'rb'))}
            response = requests.post(
                url='{:s}/matrix'.format(self.url),
                data=data,
                files=files
            )
            if response.status_code != 201:
                raise APIError(response.json())
            self.id = response.json().get('id')
            self.deleted = False
        else:
            raise ValueError("Either filepath,type or url has to be filled.")

    def delete(self):
        if not self.deleted:
            response = requests.delete(
                url='{:s}/matrix/{:d}'.format(self.url, self.id)
            )
            if response.status_code != 204:
                raise APIError(response.json())
            self.deleted = True

    def _raise_error_if_deleted(self):
        if self.deleted:
            raise ValueError('Resources deleted at host.')

    def multiply(self, other):
        """
        Multiply self with another matrix
        :param other: the matrix to be multiplied
        :type  other: CloudMatrix
        :return: the result of multiplication
        """
        self._raise_error_if_deleted()
        other._raise_error_if_deleted()
        response = requests.get(
            url='{:s}/matrix/{:d}/multiply/{:d}/'.format(self.url, self.id, other.id)
        )
        if response.status_code != 200:
            raise APIError(response.json())
        url = response.json().get('url')
        return CloudMatrix(url=self.url, id=int(url.split('/')[-1]))

    def __matmul__(self, other):
        """
        Overwrites forward matmul operator: self @ other
        :param other: other compatible types of CloudMatrix
        :return: a CloudMatrix that is the result of matrix multiplication self @ other
        """
        if not isinstance(other, CloudMatrix):
            raise ValueError("{:s} is in compatible with CloudMatrix".format(other.__class__))
        return self.multiply(other)

    def __rmatmul__(self, other):
        """
        Overwrites backward matmul operator: other @ self
        :param other: other compatible types of CloudMatrix
        :return: a CloudMatrix that is the result of matrix multiplication other @ self
        """
        if not isinstance(other, CloudMatrix):
            raise ValueError("{:s} is in compatible with CloudMatrix".format(other.__class__))
        return other.multiply(self)

    def transpose(self):
        """
        Performs matrix transpose
        :return: the transpose of self
        """
        self._raise_error_if_deleted()
        response = requests.get(
            url='{:s}/matrix/{:d}/transpose'.format(self.url, self.id)
        )
        if response.status_code != 200:
            raise APIError(response.json())
        url = response.json().get('url')
        return CloudMatrix(url=self.url, id=int(url.split('/')[-1]))

    def to_array(self):
        """
        Convert to numpy array
        :return: an numpy array that represents the matrix
        """
        self._raise_error_if_deleted()
        response = requests.get(
            url='{:s}/matrix/{:d}?alt=media'.format(self.url, self.id)
        )
        if response.status_code != 200:
            raise APIError(response.json())
        with NamedTemporaryFile(suffix=".npy") as tf:
            tf.write(response.content)
            tf.flush()
            tf.seek(0)  # reset the pointer to the head before np.load
            array = np.load(tf.file)
        return array

    def __eq__(self, other):
        """
        Overwrite the equality instance
        :param other: the right-hand side of the quality comparison
        :type other: Any        """
        if isinstance(other, CloudMatrix):
            if other.id == self.id and other.url == self.url:
                return True
        return False


class CloudMatrixManager(object):
    """
    CloudMatrixManager provides wrapper methods to operate CloudMatrices and utility methods
    to manage the matrices on cloud, including create and delete. A stack is maintained so that
    all the intermediate CloudMatrices will be stored

    Examples:
        >>> cmm = CloudMatrixManager(url=DEFAULT_URL)
        >>> nparr = np.asarray([[1,2,3,4], [5,6,7,8]])

        - Create a cloud matrices
            - Create from a file
            >>> tf = NamedTemporaryFile(suffix=".npy", delete=False)
            >>> tf.close()
            >>> np.save(tf, nparr)
            >>> cm = cmm.create_matrix(fp=tf.name, format='Numpy')
            >>> os.remove(tf.name)  # delete the unused temporary file
            >>> cm.to_array()
            array([[1, 2, 3, 4],
                   [5, 6, 7, 8]])

            - Obtain an existing matrix in cloud
            >>> cm2 = cmm.create_matrix(id=1998)
            >>> cm2.to_array()
            array([[1, 2, 3, 4],
                   [5, 6, 7, 8]])

            - Direct creation from Numpy array
            >>> cm3 = cmm.from_numpy(nparr)
            >>> cm3.to_array()
            array([[1, 2, 3, 4],
                   [5, 6, 7, 8]])

        - Perform matrix transpose
        >>> cm4 = cmm.transpose(cm)
        >>> cm3.to_array()
        array([[1, 5],
               [2, 6],
               [3, 7],
               [4, 8]])

        - Perform matrix multiplication
        >>> cm5 = cmm.matmul(cm, cm4)  # A @ A.T
        >>> cm4.to_array()
        array([[ 30,  70],
               [ 70, 174]])

        - Clean up matrix instances on the cloud. As documented in the `cloud.CloudMatrix,
            The matrices in the cloud storage will not be cleaned up automatically. You MUST
            delete them manually. In operation with `cloud.MatrixManager`, all the input and
            output CloudMatrices will be maintained in the stack of the instance of CloudMatrixManager
            that is being used. Cleaning the matrices being used had never been so easy, as
            shown as follows.
        >>> cmm.empty_stack()

    """

    def __init__(self, url):
        """
        Initializes a CloudMatrixManager
        :param url: the base url of t
        """
        self.url = url
        self.stack = set()

    def create_matrix(self, id=None, fp=None, format=None):
        if id:
            cm = CloudMatrix(url=self.url, id=id)
        elif fp:
            if not format:
                raise ValueError("Format not supplied")
            cm = CloudMatrix(url=self.url, filepath=fp, format=format)
        else:
            raise ValueError("Either id or (fp, format) must be supplied to instantiate a CloudMatrix")
        self.add_to_stack(cm)
        return cm

    def add_to_stack(self, *items):
        """ Add a list of objects to the stack
        :param items: a list of objects
        """
        self.stack.update(items)

    def empty_stack(self):
        """ Empty the stack for the manager and deletes all CloudMatrix from the cloud
        """
        for item in self.stack:
            if isinstance(item, CloudMatrix):
                item.delete()
        self.stack = set()

    def from_numpy(self, nparr):
        """ Transform given numpy-compatible matrix to CloudMatrix
        :param nparr: numpy compatible matrix
        :return: a CloudMatrix instance that reference the given matrix on cloud
        """
        nparr = np.asmatrix(nparr)
        with NamedTemporaryFile(suffix='.npy', delete=False) as tf:
            np.save(tf, nparr)
        cm = self.create_matrix(fp=tf.name, format='Numpy')
        self.add_to_stack(cm)
        os.remove(tf.name)
        return cm

    def matmul(self, cm1: CloudMatrix, cm2: CloudMatrix):
        """ Perform matrix multiplication on two CloudMatrix
        :param cm1: left-hand cloud matrix
        :param cm2: right-hand cloud matrix
        :return: a cloud matrix that represents cm1 x cm2
        """
        cm3 = cm1.multiply(cm2)
        self.add_to_stack(cm1, cm2, cm3)
        return cm3

    def transpose(self, cm: CloudMatrix):
        """
        Performs matrix transpose on the given CloudMatrix
        :param cm: cloud matrix
        :return: the transpose of cm
        """
        cmt = cm.transpose()
        self.add_to_stack(cm, cmt)
        return cmt
