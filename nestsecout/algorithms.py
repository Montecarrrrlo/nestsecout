import numpy as np
import numpy.random as rand
import nestsecout.utils as utils
from tempfile import TemporaryFile
from numpy import outer, matmul, matrix
from nestsecout.cloud import DEFAULT_URL, CloudMatrix, CloudMatrixManager

class SLSE:
    def __init__(self, A, b, l=8, p=16, q=3, url=DEFAULT_URL):
        '''
        Initializes the computation of sparse linear systems of equations
        :param A: a matrix represents the coefficients in a LSE problem
        :param b: a column vector represents the constants a LSE problem
        :param l, p, q: the default ranges of random matrices
        :param url: the url references to the cloud server
        '''

        self.A = A
        self.b = b

        self.l = l
        self.p = p
        self.q = q

        self.manager = CloudMatrixManager(url)

    def slse_transformation(self):
        '''
        Perform transformation to general SLSE problems such that it
            could be solved by Conjugate Gradient Method (CGM)
        :return: Transformed SLSE that satisfies the property of CGM
        '''
        m, n = self.A.shape

        # arbitrary positive constant between [2**l, 2**(l+q)]
        v = matrix(rand.uniform(2**self.l, 2**(self.l+self.q), size=(n,)))

        u = matrix(rand.uniform(-2**self.p, 2**self.p, size=(m,)))
        Zt_0 = outer(u, v)
        u = matrix(rand.uniform(-2**self.p, 2**self.p, size=(m,)))
        Zt_1 = outer(u, v)

        Q_0 = utils.generate_permutation_matrix(m)
        P_0 = utils.generate_permutation_matrix(n)
        T_0 = utils.generate_permutation_matrix(n)

        # Ah_0 = Q_0.T ( A + Zt_0 ) P_0.T
        # Ah_0 = self.A + Zt_0
        # Ah_0 = matmul(matmul(Q_0.T, Ah_0), P_0.T)
        Ah_0 = Q_0.T @ (self.A + Zt_0) @ P_0.T

        # Ah_1 = Q_0.T ( A + Zt_1 ) T_0
        Ah_1 = Q_0.T @ (self.A + Zt_1) @ T_0

        Ah_0T = self.manager.from_numpy(Ah_0.T)
        Ah_1 = self.manager.from_numpy(Ah_1)

        # compute at cloud
        # G = Ah_0.T * Ah_1
        G = self.manager.matmul(Ah_0T, Ah_1)

        G = G.to_array()

        self.manager.empty_stack()

        # SLSE (37)
        # A' = P_0.T * G * T_0.T - M
        # where M = Zt_0.T * A + A.T * Zt_1 + Zt_0.T * Zt_1
        # M = matmul(Zt_0.T, self.A) + matmul(self.A.T, Zt_1) + matmul(Zt_0.T, Zt_1)
        # Ap = matmul(P_0.T, matmul(G, T_0.T)) - M
        # bp = matmul(self.A.T, self.b)
        M = Zt_0.T @ self.A + self.A.T @ Zt_1 + Zt_0.T @ Zt_1
        Ap = P_0.T @ G @ T_0.T - M
        bp = self.A.T @ self.b

        return Ap, bp

    def ppcgm(self, Ap, bp, tol=1e-3):
        '''
        Performs Privacy Preserving Conjugate Gradient Method (PPCGM)
        :param tol: tolerance value of iteration
        :return: the result of the SLSE transformation
        '''
        #########################################
        # Step 1: Generate u,v,Z,P,T
        #########################################
        m,n = Ap.shape
        assert m == n, "A' must be symmetric"
        assert np.isfinite(np.linalg.cond(Ap)), "A' must be nonsingular"
        assert np.all(np.linalg.eigvals(Ap) > 0), "A' must be positive definite"

        # arbitrary positive constant between [2**l, 2**(l+q)]
        v = matrix(rand.uniform(2**self.l, 2**(self.l+self.q), size=(m,)))
        u = matrix(rand.uniform(-2**self.p, 2**self.p, size=(m,)))
        Zt = outer(u, v)
        P = utils.generate_permutation_matrix(m)
        T = utils.generate_permutation_matrix(m)

        #########################################
        # Step 2: Mask A and x
        #########################################
        # Ah' = P * (A' + Zt) * T
        Ahp = Ap + Zt
        Ahp = P @ (Ahp + Zt) @ T
        # random initial x
        x = rand.uniform(-2**self.p, 2**self.p, size=(m,))
        # xh = T.T * x
        xh = T.T @ x

        #########################################
        # Step 3: Calculate h0 on CS
        #########################################
        Ahp = self.manager.from_numpy(Ahp)
        xh = self.manager.from_numpy(xh)
        # h0 = Aph * xh
        h0 = self.manager.matmul(Ahp, xh)
        h0 = h0.to_array()

        #########################################
        # Step 4: Initialize r and p
        #########################################
        # SLSE (26)
        # r = Ap * x0 - bp
        #    = P.T * h0 - Zt * x0 - bp
        #    = P.T * P * (Ap + Zt) * x0 - Zt * x0 - bp
        # r = matmul(P.T, h0) - matmul(Zt, x) - bp
        r = P.T @ h0 - Zt @ x - bp
        p = -r

        rho = matmul(r.T, r)
        delta = tol * np.sqrt(bp.T @ bp)

        while np.sqrt(rho) > delta:
            #########################################
            # Step 5: Calculate ph1.T and ph2
            #########################################
            # ph1_T = matmul(p.T, P.T)
            # ph2 = matmul(T.T, p)
            ph1_T = p.T @ P.T
            ph2   = T.T @ p

            #########################################
            # Step 6: Calculate t and f on CS
            #########################################
            ph1_T = self.manager.from_numpy(ph1_T)
            ph2   = self.manager.from_numpy(ph2)

            # f = Ahp * ph2
            # t = ph1_T * Ahp * ph2
            #   = ph1_T & f
            f = self.manager.matmul(Ahp, ph2)
            t = self.manager.matmul(ph1_T, f)
            f = f.to_array()
            t = t.to_array()

            #########################################
            # Step 7: Calculate alpha
            #########################################
            # SLSE (28)
            # alpha = r.T * r / (t - p.T * u * v.T * p)
            # alpha = rho / (t - matmul(matmul(p.T, u), matmul(v.T, p)))
            alpha = rho / (t - (p.T @ u) @ (v.T @ p))

            #########################################
            # Step 8: Calculate new r
            #########################################
            # SLSE (30)
            # r = r + alpha * (P.T * f - u * v.T * p)
            # r = r + matmul(alpha, matmul(P.T, f) - matmul(u, matmul(v.T, p)))
            r = r + alpha @ (P.T @ f - u @ v.T @ p)
            rho_old = rho
            rho = r.T @ r

            #########################################
            # Step 9: Calculate x, beta, p
            #########################################
            # SLSE (23)
            # x = x + matmul(alpha, p)
            x = x + alpha @ p
            # SLSE (24)
            beta = rho / rho_old
            # SLSE (25)
            # p = -r + matmul(beta, p)
            p = -r + beta @ p

        self.manager.empty_stack()
        return x







